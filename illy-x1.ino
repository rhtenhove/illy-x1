#include "logo.h"
#include <Wire.h>
#include <Adafruit_SSD1306.h>

static const char VERSION[3] = "v2";

static const unsigned long HEATER_REFRESH_INTERVAL = 1000; // ms
static const unsigned long PUMP_REFRESH_INTERVAL = 1000;
static const unsigned long REQUEST_REFRESH_INTERVAL = 500;
static const unsigned long DISPLAY_REFRESH_INTERVAL = 100;

static unsigned long lastHeaterRefreshTime = 0;
static unsigned long lastPumpRefreshTime = 0;
static unsigned long lastRequestRefreshTime = 0;
static unsigned long lastDisplayRefreshTime = 0;
static char debugStr[45];

// Thermistor
int Vo;
float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

// Coffee
unsigned long ml = 0;
unsigned long mlRequested = 0;
unsigned long flowPulses = 0;
static const unsigned long pulsesPerLitre = 5600; // Datasheet states 1925-1934 per liter, but coffee changes everything. Based on manual measuring.
unsigned long brewStart = 0;
unsigned long timeBrewing = 0;

unsigned long maxBrewTime = 0;

byte pulseWasCounted = 0;
byte flowMeterState = 0;

static const unsigned long SMALL_BREW_ML = 30;
static const unsigned long LARGE_BREW_ML = 90;
static const unsigned long SMALL_BREW_MAX_TIME = 20;
static const unsigned long LARGE_BREW_MAX_TIME = 60;

// Temperature. We multiply by 10 to keep it accurate and with integers.
int temp = 0;
int tempRise = 0;
int prevTemp = 0;
int prevTempOvershoot = 0;

static const unsigned long HEATED_TEMP = 930;
static const unsigned long COOLED_TEMP = 900;
unsigned long tempOvershoot = 150;

unsigned long switchTemp = HEATED_TEMP;

// Pins
// Analog
static const unsigned int TEMP_PIN = 7; // A7

// Digital
static const unsigned int SMALL_BREW_REQUEST_PIN = 10;
static const unsigned int LARGE_BREW_REQUEST_PIN = 9;
static const unsigned int STEAM_REQUEST_PIN = 8;
static const unsigned int WATER_REQUEST_PIN = 7;

static const unsigned int HEATER_PIN = 2;
static const unsigned int PUMP_PIN = 3;
static const unsigned int BREW_VALVE_PIN = 4;
static const unsigned int STEAM_VALVE_PIN = 5;

static const unsigned int FLOWMETER_PIN = 11;

static const unsigned int POWER_LED_PIN = 6;

// Display
static const unsigned int SCREEN_WIDTH = 128;
static const unsigned int SCREEN_HEIGHT = 64;

static const signed int OLED_RESET = -1;
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup()
{
  Serial.begin(9600);

  pinMode(HEATER_PIN, OUTPUT);
  pinMode(PUMP_PIN, OUTPUT);
  pinMode(BREW_VALVE_PIN, OUTPUT);
  pinMode(STEAM_VALVE_PIN, OUTPUT);

  pinMode(POWER_LED_PIN, OUTPUT);

  digitalWrite(POWER_LED_PIN, HIGH);

  digitalWrite(HEATER_PIN, HIGH);
  digitalWrite(PUMP_PIN, HIGH);
  digitalWrite(BREW_VALVE_PIN, HIGH);
  digitalWrite(STEAM_VALVE_PIN, HIGH);

  pinMode(SMALL_BREW_REQUEST_PIN, INPUT_PULLUP);
  pinMode(LARGE_BREW_REQUEST_PIN, INPUT_PULLUP);
  pinMode(STEAM_REQUEST_PIN, INPUT_PULLUP);
  pinMode(WATER_REQUEST_PIN, INPUT_PULLUP);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  {
    Serial.println(F("SSD1306 allocation failed."));
    for (;;)
      ;
  }

  display.clearDisplay();
  display.ssd1306_command(SSD1306_DISPLAYON);
  display.drawBitmap(0, 0, coffeeLogo, 128, 64, WHITE);
  display.display();
  delay(3000);

  display.setTextColor(SSD1306_WHITE);
  display.cp437(true);

  temp = readThermistor();
  prevTemp = temp;
}

int readThermistor()
{
  Vo = analogRead(TEMP_PIN);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
  T = int((T - 273.15) * 10);
  return T;
}

void refreshDisplay()
{
  display.clearDisplay();
  display.setCursor(0, 10);

  display.setTextSize(3);
  static char tempstr[7];
  dtostrf(temp / 10, 4, 0, tempstr);
  display.write(tempstr);
  display.write((char)248);
  display.write("C\n");

  display.setTextSize(2);
  static char mlstr[10];
  sprintf(mlstr, "%3lu/%3luml\n", ml, mlRequested);
  display.write(mlstr);

  display.setTextSize(1);
  if (!digitalRead(PUMP_PIN))
  {
    timeBrewing = (millis() - brewStart) / 1000;
  }
  static char brewTimestr[7];
  sprintf(brewTimestr, "%2lu/%2lus", timeBrewing, maxBrewTime);
  display.write(brewTimestr);

  display.setCursor(SCREEN_WIDTH - (6 * strlen(VERSION)), 0);
  display.write(VERSION);

  if (!digitalRead(HEATER_PIN))
  {
    display.drawBitmap(SCREEN_WIDTH - 8, SCREEN_HEIGHT - 14, heatLogo, 8, 14, WHITE);
  }
  if (!digitalRead(PUMP_PIN))
  {
    display.drawBitmap(SCREEN_WIDTH - 8 - 12, SCREEN_HEIGHT - 14, pumpLogo, 12, 14, WHITE);
  }
  if (!digitalRead(STEAM_REQUEST_PIN))
  {
    display.drawBitmap(SCREEN_WIDTH - 8 - 12 - 13, SCREEN_HEIGHT - 12, steamLogo, 13, 12, WHITE);
  }

  display.display();
}

void brewRequested(unsigned long brewSize, unsigned long brewTime)
{
  // Stop pumping if the request switch is toggled while pumping
  if (!digitalRead(PUMP_PIN))
  {
    sprintf(debugStr, "Brew stop requested at %luml and %lus.", ml, timeBrewing);
    Serial.println(debugStr);
    mlRequested = ml;
  }
  else
  {
    sprintf(debugStr, "%luml brew requested. Max time: %lus.", brewSize, brewTime);
    Serial.println(debugStr);

    maxBrewTime = brewTime;
    flowPulses = 0;
    timeBrewing = 0;
    brewStart = millis();
    digitalWrite(BREW_VALVE_PIN, LOW); // open
    mlRequested = brewSize;
  }
}

void loop()
{
  if (millis() - lastRequestRefreshTime >= REQUEST_REFRESH_INTERVAL)
  {
    if (!digitalRead(SMALL_BREW_REQUEST_PIN))
    {
      brewRequested(SMALL_BREW_ML, SMALL_BREW_MAX_TIME);
    }
    else if (!digitalRead(LARGE_BREW_REQUEST_PIN))
    {
      brewRequested(LARGE_BREW_ML, LARGE_BREW_MAX_TIME);
    }
    lastRequestRefreshTime += REQUEST_REFRESH_INTERVAL;
  }

  flowMeterState = digitalRead(FLOWMETER_PIN);
  if (flowMeterState != pulseWasCounted)
  {
    flowPulses++;
    pulseWasCounted = flowMeterState;
  }

  // Heater
  if (millis() - lastHeaterRefreshTime >= HEATER_REFRESH_INTERVAL)
  {
    tempRise = temp - prevTemp;
    tempOvershoot = 0;
    if (tempRise > 0 && digitalRead(PUMP_PIN))
    {
      tempOvershoot = tempRise * 10;
    }
    prevTemp = temp;

    if (temp < switchTemp - tempOvershoot && digitalRead(HEATER_PIN))
    {
      switchTemp = HEATED_TEMP;
      sprintf(debugStr, "Heating up: %i/%i.", temp / 10, switchTemp / 10);
      Serial.println(debugStr);
      digitalWrite(HEATER_PIN, LOW); // on
    }
    else if (temp > switchTemp - tempOvershoot && !digitalRead(HEATER_PIN))
    {
      switchTemp = COOLED_TEMP;
      digitalWrite(HEATER_PIN, HIGH); // off
      sprintf(debugStr, "Cooling down: %i/%i.", temp / 10, switchTemp / 10);
      Serial.println(debugStr);
    }
    lastHeaterRefreshTime += HEATER_REFRESH_INTERVAL;
  }

  // Pump
  if (millis() - lastPumpRefreshTime >= PUMP_REFRESH_INTERVAL)
  {
    if (timeBrewing < maxBrewTime)
    {
      if (ml < mlRequested && digitalRead(PUMP_PIN))
      {
        sprintf(debugStr, "Starting pump. ml: %lu/%lu.", ml, mlRequested);
        Serial.println(debugStr);

        digitalWrite(PUMP_PIN, LOW); // on
      }
      else if ((ml >= mlRequested && !digitalRead(PUMP_PIN)))
      {
        sprintf(debugStr, "Stopping pump. ml: %lu/%lu.", ml, mlRequested);
        Serial.println(debugStr);

        digitalWrite(PUMP_PIN, HIGH);        // off
        digitalWrite(BREW_VALVE_PIN, HIGH);  // closed
        digitalWrite(STEAM_VALVE_PIN, HIGH); // closed
      }
    }
    else if (!digitalRead(PUMP_PIN))
    {
      sprintf(debugStr, "Stopping pump. %lu/%lus.", timeBrewing, maxBrewTime);
      Serial.println(debugStr);

      digitalWrite(PUMP_PIN, HIGH);        // off
      digitalWrite(BREW_VALVE_PIN, HIGH);  // closed
      digitalWrite(STEAM_VALVE_PIN, HIGH); // closed
    }
    lastPumpRefreshTime += PUMP_REFRESH_INTERVAL;
  }

  // Display
  if (millis() - lastDisplayRefreshTime >= DISPLAY_REFRESH_INTERVAL)
  {
    temp = readThermistor();
    ml = flowPulses * 1000 / pulsesPerLitre;
    refreshDisplay();
    lastDisplayRefreshTime += DISPLAY_REFRESH_INTERVAL;
  }
  delay(10);
}
