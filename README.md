## Contains

- SSD1306 display
- Thermistor reading
- Flow meter reading
- Power LED
- Automatic valve operation

## TODO

- Steam and hot water dispensing
- Ensuring water flow when pumping
- Visual feedback of states of valves and components
- Versioning

### Some notes

- 10k resistor for thermistor

